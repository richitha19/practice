from tkinter import *
import sqlite3
conn = sqlite3.connect('users.db')
c = conn.cursor()
c.execute('''CREATE TABLE if not exists users
          (id integer primary key,username text,password text)''')
conn.commit()
# Designing window for registration
def register():
    global register_screen
    register_screen = Toplevel(main_screen)
    register_screen.configure(bg="sea green")
    register_screen.title("Register")
    register_screen.geometry("300x250")

    global username
    global password
    global username_entry
    global password_entry
    username = StringVar()
    password = StringVar()

    Label(register_screen, text="Please enter details below", bg="pink").pack()
    Label(register_screen, text="").pack()
    username_label = Label(register_screen, text="Username * ", bg="grey")
    username_label.pack()
    username_entry = Entry(register_screen, textvariable=username)
    username_entry.pack()
    password_label = Label(register_screen, text="Password * ", bg="grey")
    password_label.pack()
    password_entry = Entry(register_screen, textvariable=password, show='*')
    password_entry.pack()
    Label(register_screen, text="").pack()
    Button(register_screen, text="Register", width=10, height=1, bg="pink", command=register_user).pack()


# Designing window for login
def login():
    global login_screen
    login_screen = Toplevel(main_screen)
    login_screen.configure(bg="sea green")
    login_screen.title("Login")
    login_screen.geometry("300x250")
    Label(login_screen, text="Please enter details below to login").pack()
    Label(login_screen, text="").pack()

    global username_verify
    global password_verify

    username_verify = StringVar()
    password_verify = StringVar()

    global username_login_entry
    global password_login_entry

    Label(login_screen, text="Username * ").pack()
    username_login_entry = Entry(login_screen, textvariable=username_verify)
    username_login_entry.pack()
    Label(login_screen, text="").pack()
    Label(login_screen, text="Password * ").pack()
    password_login_entry = Entry(login_screen, textvariable=password_verify, show='*')
    password_login_entry.pack()
    Label(login_screen, text="").pack()
    Button(login_screen, text="Login", width=10, height=1, bg="pink", command=login_verify).pack()


# Implementing event on register button
def register_user():
    username_info = username.get()
    password_info = password.get()

    # You can perform actions here like saving to a database
    # For simplicity, let's just print the details for now
    print("Username:", username_info)
    print("Password:", password_info)

    username_entry.delete(0, END)
    password_entry.delete(0, END)

    Label(register_screen, text="Registration Success", fg="black", font=("calibri", 14)).pack()


# Implementing event on login button
def login_verify():
    username1 = username_verify.get()
    password1 = password_verify.get()
    username_login_entry.delete(0, END)
    password_login_entry.delete(0, END)

    # Here you can perform actions like checking against a database
    # For simplicity, let's just print the login details for now
    print("Login attempt with Username:", username1)
    print("Password:", password1)


# Designing Main(first) window
def main_account_screen():
    global main_screen
    main_screen = Tk()
    main_screen.configure(bg="sea green")
    main_screen.geometry("300x250")
    main_screen.title("Account Login")
    Label(text="Select Your Choice", bg="light pink", width="300", height="2", font=("Calibri", 16)).pack()
    Label(text="").pack()
    Button(text="Login", height="3", width="30", command=login, bg="light pink", font=("calibri", 14)).pack()
    Label(text="").pack()
    Button(text="Register", height="3", width="30", command=register, bg="light pink", font=("calibri", 14)).pack()

    main_screen.mainloop()


main_account_screen()
